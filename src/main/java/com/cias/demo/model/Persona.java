/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cias.demo.model;

import java.time.LocalDateTime;
import lombok.Data;

/**
 *
 * @author oswal
 */
 @Data
public class Persona {
    int id;
    String nombre;
    String primer_apellido;
    String segundo_apellido;
    String telefono;
    String estatus;
    LocalDateTime fecha_ins;
    LocalDateTime  fecha_upd;
}
