/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cias.demo.model;

import lombok.Data;

/**
 *
 * @author oswal
 */
@Data
public class ServiceResponse {
    Boolean success;
    String message;
}
