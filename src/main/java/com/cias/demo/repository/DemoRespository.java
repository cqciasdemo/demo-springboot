/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cias.demo.repository;

import com.cias.demo.model.Persona;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author oswal
 */
@Repository
public class DemoRespository implements IDemoRepository {
        
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public List<Persona> findAll() {
        String SQL = "SELECT * FROM Persona where estatus =1";
        //return jdbcTemplate.query(SQL,BeanPropertyRowMapper.newInstance(Persona.class) );
        return jdbcTemplate.query(SQL, new PersonaRowMapper());
    }
        
    private static class PersonaRowMapper implements RowMapper<Persona> {
        public Persona mapRow(ResultSet rs, int rowNum) throws SQLException {
            Persona persona = new Persona();
            persona.setId(rs.getInt("id"));
            persona.setNombre(rs.getString("nombre"));
            persona.setPrimer_apellido(rs.getString("primer_apellido"));
            persona.setSegundo_apellido(rs.getString("segundo_apellido"));
            persona.setTelefono(rs.getString("telefono"));
            persona.setEstatus(rs.getString("estatus"));

            // Obtener la columna datetimeoffset y convertirla a LocalDateTime
            OffsetDateTime offsetDateTime = rs.getObject("fecha_ins", OffsetDateTime.class);
            if (offsetDateTime != null) {
                LocalDateTime localDateTime = offsetDateTime.toLocalDateTime();
                persona.setFecha_ins(localDateTime);
            }

            // Obtener la columna datetimeoffset y convertirla a LocalDateTime
            offsetDateTime = rs.getObject("fecha_upd", OffsetDateTime.class);
            if (offsetDateTime != null) {
                LocalDateTime localDateTime = offsetDateTime.toLocalDateTime();
                persona.setFecha_upd(localDateTime);
            }
            return persona;
        }
    }
    
    @Override
    public int save(Persona persona) {
        String SQL = "INSERT INTO Persona(nombre,primer_apellido,segundo_apellido,telefono,fecha_ins) VALUES (?,?,?,?,?)";
        return jdbcTemplate.update(SQL,new Object[]{
            persona.getNombre(),
            persona.getPrimer_apellido(),
            persona.getSegundo_apellido(),
            persona.getTelefono(),
            persona.getFecha_ins()
        });
    }

    @Override
    public int update(Persona persona) {
        String SQL = "UPDATE Persona SET nombre = ?,primer_apellido = ?,segundo_apellido = ?,telefono = ?,fecha_upd = ? WHERE id = ?";
        return jdbcTemplate.update(SQL,new Object[]{
                persona.getNombre(),
                persona.getPrimer_apellido(),
                persona.getSegundo_apellido(),
                persona.getTelefono(),
                persona.getFecha_upd(),
                persona.getId()
        });
    }

    @Override
    public int deleteById(int id) {
    String SQL = "UPDATE Persona SET estatus = 0 WHERE id=?";
    return jdbcTemplate.update(SQL,new Object[]{id});
    }
    
}
