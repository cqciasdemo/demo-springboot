/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cias.demo.repository;

import com.cias.demo.model.Persona;
import java.util.List;

/**
 *
 * @author oswal
 */
public interface IDemoRepository {
    public List<Persona> findAll();
    public int save(Persona persona);
    public int update(Persona persona);
    public int deleteById(int id);
}
