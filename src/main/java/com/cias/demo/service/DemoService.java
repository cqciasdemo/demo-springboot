/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cias.demo.service;

import com.cias.demo.model.Persona;
import com.cias.demo.repository.IDemoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author oswal
 */
@Service
public class DemoService implements IDemoService {
    
    @Autowired
    private IDemoRepository iDemoRepository;
    
    @Override
    public List<Persona> findAll() {
    
       List<Persona> list;
       try{
           list = iDemoRepository.findAll();
       }catch(Exception ex){
           throw ex;
       }
       return list;
    }

    @Override
    public int save(Persona persona) {
        int row;
        try {
            row = iDemoRepository.save(persona);
        }catch (Exception ex){
            throw ex;
        }
        return row;
    }

    @Override
    public int update(Persona persona) {
        int row;
        try {
            row = iDemoRepository.update(persona);
        }catch (Exception ex){
            throw ex;
        }
        return row;
    }

    @Override
    public int deleteById(int id) {
        int row;
        try {
            row = iDemoRepository.deleteById(id);
        }catch (Exception ex){
            throw ex;
        }
        return row;
    }
    
}
