/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cias.demo.controller;

import com.cias.demo.model.Persona;
import com.cias.demo.model.ServiceResponse;
import com.cias.demo.service.IDemoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author oswal
 */
@RestController
@RequestMapping("api/v1/demo")
@CrossOrigin("*")
public class DemoController {
    
    @Autowired
    private IDemoService iDemoService;

    @GetMapping("/list")
    public ResponseEntity<List<Persona>> list(){
        var result = iDemoService.findAll();
        return new ResponseEntity<>(result,HttpStatus.OK );
    }

    @PostMapping("/save")
    public ResponseEntity<ServiceResponse> save(@RequestBody Persona persona){
        ServiceResponse serviceResponse = new ServiceResponse();
        int result = iDemoService.save(persona);
        if(result==1){
            serviceResponse.setSuccess(true);
            serviceResponse.setMessage("Guardado");
        }else{
            serviceResponse.setSuccess(false);
            serviceResponse.setMessage("Error");
        }
        return new ResponseEntity<>(serviceResponse,HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<ServiceResponse> update(@RequestBody Persona persona){
        ServiceResponse serviceResponse = new ServiceResponse();
        int result = iDemoService.update(persona);
        if(result==1){
            serviceResponse.setSuccess(true);
            serviceResponse.setMessage("Editado");
        }else{
            serviceResponse.setSuccess(false);
            serviceResponse.setMessage("Error");
        }
        return new ResponseEntity<>(serviceResponse,HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<ServiceResponse> update(@PathVariable int id){
        ServiceResponse serviceResponse = new ServiceResponse();
        int result = iDemoService.deleteById(id);
        if(result==1){
            serviceResponse.setSuccess(true);
            serviceResponse.setMessage("Eliminado");
        }else{
            serviceResponse.setSuccess(false);
            serviceResponse.setMessage("Error");
        }
        return new ResponseEntity<>(serviceResponse,HttpStatus.OK);
    }
}
